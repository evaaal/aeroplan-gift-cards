wlog("Data Sourcing from DB2",level=1)
wlog("========================",level=1)


#Mismapping of TEST and PROD
getDataFromServer(db2,sqlFile = 'sql/gc_i.sql', name = "gc_i", convert_types = F)
getDataFromServer(db2,sqlFile = 'sql/gc_r.sql', name = "gc_r", convert_types = F)
# 
# if(class(gc_ti$val)!="numeric")gc_ti %<>%
#   dplyr::mutate(
#     requestdate = as.POSIXct(requestdate),
#     issue_date = as.POSIXct(issue_date),
#     topup_date = as.POSIXct(topup_date),
#     createdate = as.POSIXct(createdate),
#     transtart = as.POSIXct(transtart),
#     isvoidedtr = ifelse(isvoidedtr=='f',0,1),
#     isvoidedti = ifelse(isvoidedti=='f',0,1),
#     val = as.numeric(val),
#     amount = as.numeric(amount)/100,
#     amount = balance/100,
#     issueamount = issueamount/100,
#     sellingprice = as.numeric(sellingprice)/100,
#     new_client = ifelse(createdate<=issue_date,ifelse(contactid=="C6UJ9A007BEV","Unknown","Former"),"New")
#   )%>%dplyr::filter(issue_date>='2019-08-01')


if(class(gc_i$issue_date)[1]!="POSIXct")gc_i %<>%
  dplyr::mutate(
    requestdate = as.POSIXct(requestdate),
    issue_date = as.POSIXct(issue_date),
    topup_date = as.POSIXct(topup_date),
    businessdate = as.POSIXct(businessdate),
    amount = amount/100,
    balance = balance/100,
    issueamount = issueamount/100,
    transtart = as.POSIXct(transtart))%>%dplyr::filter(issue_date>='2019-08-01')


if(class(gc_r$val)!="numeric")gc_r %<>%
  dplyr::mutate(
    requestdate = as.POSIXct(requestdate),
    issue_date = as.POSIXct(issue_date),
    topup_date = as.POSIXct(topup_date),
    createdate = as.POSIXct(createdate),
    transtart = as.POSIXct(transtart),
    isvoidedtr = ifelse(isvoidedtr=='f',0,1),
    isvoidedti = ifelse(isvoidedti=='f',0,1),
    val = as.numeric(val),
    amount = as.numeric(amount)/100,
    balance = as.numeric(balance)/100,
    issueamount = issueamount/100,
    sellingprice = as.numeric(sellingprice)/100,
    new_client = ifelse(createdate<=issue_date,ifelse(contactid=="C6UJ9A007BEV","Unknown","Former"),"New")
  )%>%dplyr::filter(issue_date>='2019-08-01')


return(class(gc_r$val)=="numeric")