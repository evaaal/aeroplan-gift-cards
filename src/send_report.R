wlog("Send E-mails to Marketing", head = T)
wlog("=========================")

#Production-ready mass e-mail
if (RUN_ENV == "PROD")
{
  #Identify the managers for a given store
  #Gather the report created for the corresponding
  email_body <- sendmailR::mime_part(email)
  email_body[["headers"]][["Content-Type"]] <- "text/html"
  
  if (nrow(cards_redeemed) > 0)
    c_send_report <- tryCatch(
      expr = {
        wlog("Distributing report...", head = T)
        wlog(level = 3)
        # mailstatus <- sendEmail(
        #   recipientList = paste0(USER,"@harryrosen.com"),
          mailstatus <- sendEmail(recipientList = RECIPS
          ,
          subject = 'Aeroplan Gift Card Report',
          bcc = paste0(USER,"@harryrosen.com"),
          body = email_body,
          smtp = '142.215.51.33'
        )
        wlog(paste0("mail send status code:     ", mailstatus$code), level = 3)
        wlog(paste0("mail send status message:  ", mailstatus$mdg),
             level = 3)
        wlog("E-mail Sent Successfully!")
        mailstatus$code %in% c("220", "221")
      },
      error = function(err) {
        wlog("An error occurred sending the email out")
        wlog(err, level = 2)
        return(FALSE)
      }
    )
  
}

return(c_send_report)
