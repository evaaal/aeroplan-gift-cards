with gc as
(select TO_DATE(l.REQUESTDATE,'YYYY-MM-DD HH24:MI:SS') REQUESTDATE,
  l.AMOUNT,
  r.DESCRIPTION,
  gc.BALANCE,
  gca.GIFTCARDID,
  gca.GIFTCARDNO,
  gc.ISSUEAMOUNT,
  TO_DATE(gc.ISSUE_DATE,'YYYY-MM-DD HH24:MI:SS') ISSUE_DATE,
  TO_DATE(gc.TOPUP_DATE,'YYYY-MM-DD HH24:MI:SS') TOPUP_DATE,
  te.FKSTORENO,
  TO_DATE(tr.BUSINESSDATE,'YYYY-MM-DD HH24:MI:SS') BUSINESSDATE,
  TO_DATE(tr.TRANSTART,'YYYY-MM-DD HH24:MI:SS') TRANSTART
  
  from hri.gc_aeroplan gca
  --Gather the relevant giftcard information such as the date issued and amount
  inner join vstore.GIFTCARD gc on gc.PKGIFTCARDNO = gca.giftcardno
  inner join vstore.giftcardlog l on gc.pkgiftcardno = l.fkgiftcardno
  inner join vstore.terminal te on te.terminalno = l.terminalno
  inner join vstore.transaction tr
  on tr.fkterminalid = te.pkterminalid and tr.tranno = l.tranno and te.fkstoreno = l.storeno
  inner join vstore.request r on r.pkrequestno = l.fkrequestno
  where nvl(l.fkrequestno,1) = 2)
select *
  from gc;