with gc as
(select c.CONTACTID,
se.customerid customerid,
  c.lastname || ', '||c.firstname contactname,
  c.account,
  TO_DATE(l.REQUESTDATE,'YYYY-MM-DD HH24:MI:SS') REQUESTDATE,
  l.AMOUNT,
  r.DESCRIPTION,
  gc.BALANCE,
  gca.GIFTCARDID,
  gca.GIFTCARDNO,
  gc.ISSUEAMOUNT,
  TO_DATE(gc.ISSUE_DATE,'YYYY-MM-DD HH24:MI:SS') ISSUE_DATE,
  TO_DATE(gc.TOPUP_DATE,'YYYY-MM-DD HH24:MI:SS') TOPUP_DATE,
  TO_DATE(c.CREATEDATE,'YYYY-MM-DD HH24:MI:SS') CREATEDATE,
  te.FKSTORENO,
  srl.storename,
  to_char(tr.PKTRANSACTIONID) PKTRANSACTIONID,
  TO_DATE(tr.BUSINESSDATE,'YYYY-MM-DD HH24:MI:SS') BUSINESSDATE,
  TO_DATE(tr.TRANSTART,'YYYY-MM-DD HH24:MI:SS') TRANSTART,
  to_char(ti.PKTRANITEMID) PKTRANITEMID,
  tr.ISVOIDED isvoidedtr,
  ti.ISVOIDED isvoidedti,
  st.div_cd                                   dept,
  st.deptdesc                                 deptdesc,
  st.dept_cd                                  subd,
  trim(replace(st.classdsc, st.deptdesc, '')) brand,
  st.description                              subcl,
  st.style_id,
  ti.qty qty,
  ti.SELLINGPRICE,
  ti.SELLINGPRICE * ti.QTY / 100        val
  from hri.gc_aeroplan gca
  --Gather the relevant giftcard information such as the date issued and amount
  inner join vstore.GIFTCARD gc on gc.PKGIFTCARDNO = gca.giftcardno
  inner join vstore.giftcardlog l on gc.pkgiftcardno = l.fkgiftcardno
  inner join vstore.terminal te on te.terminalno = l.terminalno
  inner join vstore.transaction tr
  on tr.fkterminalid = te.pkterminalid and tr.tranno = l.tranno and te.fkstoreno = l.storeno
  inner join vstore.request r on r.pkrequestno = l.fkrequestno
  inner join vstore.customer cu on tr.fkcustomerno = cu.pkcustomerno
  inner join crm.salesentity se on se.customerid = cu.identificationno
  inner join crm.contact c on c.contactid = se.contactid
  left join vstore.tranitem ti on tr.PKTRANSACTIONID = ti.FKTRANSACTIONID
  left join (select FKTRANITEMID, sum(AMOUNT) amount
             from vstore.TRANITEMDISCOUNT
             group by FKTRANITEMID) tid
  on tid.fktranitemid = ti.PKTRANITEMID
  left join vstore.sku k on k.pksku = ti.fksku
  left join custom.style st on st.style_id = k.fkstyleno
  inner join hri.store_region_levels srl on te.fkstoreno = srl.sc_numeric
  where l.fkrequestno = 3 and c.seccodeid in ('F6UJ9A000002','F6UJ9A000004')
  order by c.CREATEDATE desc)
select *
  from gc