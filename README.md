# Aeroplan x HRI Gift Cards Report

One Paragraph of project description goes here

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them.  This assuming you already have [R](https://cran.r-project.org/bin/windows/base/) and [RStudio](https://rstudio.com/products/rstudio/download/)

```
pkgs <-
    c('purrr',
      'dplyr',
      'magrittr',
      'tibble', 
      'reshape2',
      'knitr',
      'kableExtra',
      'ggplot2')
  
  invisible(suppressPackageStartupMessages(sapply(pkgs, require, character.only = T))) ->
    lib.out
    #Determining which libraries are missing and need to be installed
    c_lib_and_db <- ifelse(abs(sum(lib.out) - length(lib.out)), {
    tryCatch({
      install.packages(names(lib.out)[!lib.out], dependencies = T)
      return(T)
    }, error = function(err)
      F)
  }, T)

```

This code allows you to load all available libraries and to test downloading from CRAN if the libraries do not exist on the machine

#### Files/Folders Required  

The following are some of the files and folders you may need to have this project up and running:  

 * config.R - [Most updated version on bitbucket](https://bitbucket.org/hrianalytics/r-project/src/master/config.R)  
 * config.json - Configuration file (defined below)  
 * secret - Folder on the R drive directory including TNS Names and Database configuration addresses  
 

### Config

The following are a set of configs necessary within the directory  

```json
 "SQL_DIR":"sql",
 
 "TZ_CONFIG":"GMT",

  "USER":"evalencia",
  
  "RECIPS":"dthompson@harryrosen.com",

  "STORES":["00007","00008","00011","00112","00020","00022","00025","00027","00033","00034","00035","00043","00044","00050","00055","00057","00060","00061","00065"]
  
```


## Cron Schedule

At the moment, the job is assigned to run everyone Monday at 9AM to RECIPS defined above.

